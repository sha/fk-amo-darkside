"use strict";

function click(e) {
  window[e.target.dataset.action](e.target.dataset.key);
  //window.close();
}

var addevents = function() {
  document.addEventListener("DOMContentLoaded", function() {
    var divs = document.querySelectorAll(".itemLabel");
    for (var i = 0; i < divs.length; i++) {
      divs[i].addEventListener("click", click);
    }
  });
};

var accounts = {};

var getacconts = function(){
    var accounts = {};
    fetch("http://white.bpmcenter.pro/api/v2/customers/list",{
        headers: {
            'Access-Control-Allow-Origin':'*'
        }
    })
    .then(function(response) {
        response.json().then(function(data) {
        data.forEach(function(el) {
            accounts[el.api_key] = {
            domain: el.amo_domain,
            user: el.amo_login,
            hash: el.amo_hash
            };

            var item = document.createElement("li");
            item.className = "item mdl-button mdl-js-button mdl-js-ripple-effect";
            item.innerHTML = `
                    <div class="hbox center fullHeight">
                        <a class="itemLabel" data-key="${
                        el.api_key
                        }" data-action="login">${el.name}</a>
                        <a class="itemLabel" data-key="${
                        el.api_key
                        }" data-action="logout"><i class="material-icons">close</i></a>
                    </div>`;
            var aitems = item.getElementsByTagName("a");
            var i;
            for (i = 0; i < aitems.length; i++) {
            aitems[i].addEventListener("click", click);
            }
            document.getElementById("popupMenu").appendChild(item);
        });
        addevents();
        });
    })
    .catch(function(err) {
        console.error("Fetch Error :-S", err);
    });
    return accounts;
}

var accounts = getacconts();

var logout = function(data) {
  var auth = accounts[data];

  var url = `https://${auth.domain}/?logout=yes`;
  fetch(url)
    .then(function() {
      chrome.tabs.query({ url: "*://*.amocrm.ru/*" }, function(results) {
        if (results.length != 0) {
          results.forEach(function(el) {
            chrome.tabs.remove(el.id);
          });
        }
      });
    })
    .catch(function(err) {
      console.error("Fetch Error :-S", err);
    });
};

var login = function(key) {
  var auth = accounts[key];

  var url = `https://${auth.domain}/private/api/auth.php?type=json&USER_LOGIN=${
    auth.user
  }&USER_HASH=${auth.hash}`;

  fetch(url)
    .then(function(response) {
      console.log(response);
      //window.location.replace(`https://${auth.domain}`);
      chrome.tabs.create({ url: `https://${auth.domain}` });
      window.close();
    })
    .catch(function(err) {
      console.error("Fetch Error :-S", err);
    });
};
